# Jenkinsfile リポジトリー

２つの環境に応じたブランチが存在します。

開発環境には、testブランチを利用する。

本番環境は、masterを利用している。

開発環境ではテスト完了している状態でリリースする仕組みはtestブランチをmasterブランチにマージさせる。

# TCarpRDF関連の関連の実行フロー


## TCarpRDFの通常実行（自動実行）

### 1. TCarpRDF実行前に、GlyTouCanデータを更新する処理

`JenkinsFile.tcarprdf-gtc-update`

```perl
node {
	stage('update_tcarprdf_data') {
		sh '/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/bin/LocalGlyTouCanID > glytoucan.tsv '
	}
}
```

### 2. TCarpRDF の週１回の実行、毎週実施

`JenkinsFile.tcarprdf-update`

```perl
node {
	stage('update_tcarprdf_data') {
		//sh '/glycosite/jenkins/workspace/tcarp/tcarprdf/bin/TCarpRDF update '
		sh '/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/bin/TCarpRDF update '
		
	}
}
```

### 3. TCarpRDF実行後に、isql load で virtuosoにRDFをロードする

`JenkinsFile.tcarp`
* https://gitlab.com/glyconavi/jenkinsfile/-/blob/master/JenkinsFile.tcarp

```perl
node {
	stage('load_tcarprdf') {
		sh 'docker exec -i glycosite-virtuoso chmod +x /scripts/isql_tcarp.sh '
		sh 'docker exec -i glycosite-virtuoso /scripts/isql_tcarp.sh '
	}
}
```

* https://gitlab.com/glyconavi/noguchi/docker-virtuoso/-/blob/master/scripts/isql_tcarp.sh



### 4. TCarpRDFを木曜0時に実行したあとの、個別ファイルの一括圧縮操作

`JenkinsFile.tcarprdf-compress`

```perl
node {
	stage('compress_tcarprdf_data') {
		sh 'docker exec -i glycosite-virtuoso chmod +x /scripts/compress_tcarp.sh '
		sh 'docker exec -i glycosite-virtuoso /scripts/compress_tcarp.sh '
	}
}
```

* https://gitlab.com/glyconavi/noguchi/docker-virtuoso/-/blob/master/scripts/compress_tcarp.sh



### 5. TCarpRDF実行後のRDF一括圧縮および個別データを個別データをpublicで公開するためにデータを移動

`JenkinsFile.tcarprdf-xxxxxx`

* Path: `https://public.glyconavi.org/data/pub/tcarp/{*ttl.gz or *.tar.gz}`

```perl
node {
    
}
```






## 通常実行以外の処理で、手動実行する処理


### A. TCarpRDF 全件実行、初回起動時に一回実施

`JenkinsFile.tcarprdf-all-update`

* https://gitlab.com/glyconavi/jenkinsfile/-/blob/master/JenkinsFile.tcarprdf-all-update

```perl
node {
	stage('update_tcarprdf_data') {
		sh '/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/bin/TCarpRDF update -a '
	}
}
```




### B. TCarpRDF で、トラブルなどがあった場合トラブルなどがあった場合　データの同期をスキップして全てのローカルデータの解析を実行

`JenkinsFile.tcarprdf-update-all-skip-sync`

```perl
node {
	stage('update_tcarprdf_data') {
		sh '/glycosite/virtuoso/data/pdb/tcarp/tcarprdf/bin/TCarpRDF update -a --skip-sync '
	}
}
```

